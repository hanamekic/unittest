﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3.KonverzijaValuta
{
    public class DummyExchangeRate : IExchangeRate
    {

        public decimal ExchangeRate(string from, string to)
        {
            return 0.5m;
        }
    }
}
