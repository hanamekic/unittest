﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3.KonverzijaValuta
{
    public class CurrencyConverter
    {
        IExchangeRate _exchageRate;

        public CurrencyConverter()
        {
            _exchageRate = new ExchangeRateService();

        }
#if DEBUG
        /// <summary>
        /// Constructor will be used only in debug mode
        /// </summary>
        /// <param name="rate"></param>
        public CurrencyConverter(IExchangeRate rate)
        {
            _exchageRate = rate;
        }
#endif
        public decimal Convert(decimal amount, string from, string to)
        {
            if (amount < 0)
            {
                throw new ArgumentException("Amount cannot be negative");
            }

            decimal rate = _exchageRate.ExchangeRate(from, to);

            if (rate <= 0)
            {
                throw new InvalidOperationException("Invalid exchage returned by service");
            }


            decimal result = _exchageRate.ExchangeRate(from, to) * amount;
            // adding some logic
            if (result < 1000)
            {
                return result - result * 0.01m;
            }
            return result;
        }
    }
}
