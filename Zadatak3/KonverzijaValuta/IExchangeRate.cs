﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3.KonverzijaValuta
{
    public interface IExchangeRate
    {
        decimal ExchangeRate(string from, string to);
    }
}
