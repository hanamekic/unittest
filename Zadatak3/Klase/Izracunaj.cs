﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    public class Izracunaj
    {
        public Izracunaj() { }

        public Double izracunajCijenuBezTakse(Double distanca, Double tezina, Double hitnost)
        {
            Double c = (distanca / 95643) + tezina * ((1 + hitnost * 0.1) * distanca) * 0.00055;
            return System.Math.Round(c, 2);
        }

        public Double izracunajCijenuSaTaksom(Double distanca, Double tezina, Double hitnost, Double takse)
        {
            Double cBT = izracunajCijenuBezTakse(distanca, tezina, hitnost);
            Double c = cBT + (cBT * takse);
            return System.Math.Round(c, 2);
        }

        public Double izracunajDistancu (Double tezina, Double hitnost, Double takse, Double cijena)
        {
            Double cijenaBT = cijena / (1 + takse);
            Double distanca = cijenaBT / ((1 / 95643) + tezina * (1 + hitnost * 0.1) * 0.00055);
            distanca = System.Math.Round(distanca, 2);
            return distanca;
        }

        public Double izracunajTezinu (Double distanca, Double hitnost, Double takse, Double cijena)
        {
            Double cijenaBT = cijena / (1 + takse);
            Double tezina = (cijenaBT - (distanca / 95643)) / (((1 + hitnost * 0.1) * distanca) * 0.00055);
            tezina = System.Math.Round(tezina, 2);
            return tezina;
        }

        public Double izracunajTakse(Double distanca, Double tezina, Double hitnost, Double cijena)
        {
            Double cijenaBT = (distanca / 95643) + tezina * ((1 + hitnost * 0.1) * distanca) * 0.00055;
            Double takse = (cijena / cijenaBT) - 1;
            takse = System.Math.Round(takse, 2);
            return takse;
        }

    }
}
