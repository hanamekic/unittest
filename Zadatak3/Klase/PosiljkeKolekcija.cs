﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Zadatak3
{
    public class PosiljkeKolekcija
    {

        private List<Posiljka> _posiljke;

        #region KONSTRUKTOR, GET, SET

        public PosiljkeKolekcija()
        {
            this._posiljke = new List<Posiljka>();
        }

        public List<Posiljka> Posiljke
        {
            get { return _posiljke; }
            set { _posiljke = value; }
        }

        #endregion

        #region FUNKCIJE

        public void dodajPosiljku (Posiljka p)
        {
            _posiljke.Add(p);
        }

        public void obrisiPosiljku(Posiljka p)
        {
            _posiljke.Remove(p);
        }

        public void obrisiPoIndexu(Int32 i)
        {
            _posiljke.RemoveAt(i);
        }

        public Boolean imaLiPosiljki()
        {
            if (_posiljke.Count == 0)
                return false;

            return true;
        }

        #endregion

        #region XML

        public void Serijalizacija()
        {
            FileStream fs = new FileStream("krediti.xml", FileMode.Create);
            XmlSerializer x = new XmlSerializer(typeof(List<Posiljka>));
            x.Serialize(fs, _posiljke);
            fs.Close();
        }

        public void Deserijalizacija()
        {
            if (File.Exists("krediti.xml"))
            {
                FileStream fs = new FileStream("krediti.xml", FileMode.Open);
                XmlSerializer x = new XmlSerializer(typeof(List<Posiljka>));
                _posiljke = (List<Posiljka>)x.Deserialize(fs);
                fs.Close();
            }
        }

        #endregion
    }
}
