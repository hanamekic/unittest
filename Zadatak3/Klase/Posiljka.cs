﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Zadatak3
{
    public class Posiljka
    {
        private static readonly Random getrandom = new Random();

        Int32 id;
        String ime, prezime, jmbg, telefon, komentar;
        String odakle, dokle;
        Double distanca, tezina, takse, hitnost, cijenaBezTakse, cijenaSaTaksom;

        #region KONSTRUKTORI

        public Posiljka() { }

        public Posiljka(String o, String d, String dis, String tez, String tak, Double h, String c)
        {
            try
            {
                if (!validacijaOdakle(o))
                    throw new ArgumentException("Pogrešan unos. Lokacija može sadržavati samo slova.");

                this.odakle = o;
                 
                if (!validacijaDokle(d))
                    throw new ArgumentException("Pogrešan unos. Lokacija može sadržavati samo slova.");

                this.dokle = d;

                if (!validacijaDistanca(dis))
                    throw new ArgumentException("Pogrešan unos distance! Distanca mora biti numerička vrijednost veća od nule.");

                this.distanca = Convert.ToDouble(dis);

                if (!validacijaTezina(tez))
                    throw new ArgumentException("Pogrešan unos težine! Težina mora biti numerička vrijednost veća od nule.");

                this.tezina = Convert.ToDouble(tez);

                if (!validacijaTakse(tak))
                    throw new ArgumentException("Pogrešan unos taksi! Takse moraju biti numerička vrijednost veća od nule.");

                this.takse = Convert.ToDouble(tak);
                this.hitnost = Convert.ToDouble(h);


                if (!validacijaCijena(c))
                    throw new ArgumentException("Pogrešan unos cijene! Cijena mora biti numerička vrijednost veća od nule.");

                this.cijenaSaTaksom = Convert.ToDouble(c);
                Izracunaj izracun = new Izracunaj();
                this.cijenaBezTakse = izracun.izracunajCijenuBezTakse(distanca, tezina, hitnost);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Posiljka(String o, String d, String dis, String tez, String tak, Double h, String c,
            String i, String p, String j, String tel, String kom)
        {
            try
            {
                if (!validacijaOdakle(o))
                    throw new ArgumentException("Pogrešan unos. Lokacija može sadržavati samo slova.");

                this.odakle = o;

                if (!validacijaDokle(d))
                    throw new ArgumentException("Pogrešan unos. Lokacija može sadržavati samo slova.");

                this.dokle = d;

                if (!validacijaDistanca(dis))
                    throw new ArgumentException("Pogrešan unos distance! Distanca mora biti numerička vrijednost veća od nule.");

                this.distanca = Convert.ToDouble(dis);

                if (!validacijaTezina(tez))
                    throw new ArgumentException("Pogrešan unos težine! Težina mora biti numerička vrijednost veća od nule.");

                this.tezina = Convert.ToDouble(tez);

                if (!validacijaTakse(tak))
                    throw new ArgumentException("Pogrešan unos taksi! Takse moraju biti numerička vrijednost veća od nule.");

                this.takse = Convert.ToDouble(tak);
                this.hitnost = Convert.ToDouble(h);


                if (!validacijaCijena(c))
                    throw new ArgumentException("Pogrešan unos cijene! Cijena mora biti numerička vrijednost veća od nule.");

                this.cijenaSaTaksom = Convert.ToDouble(c);
                Izracunaj izracun = new Izracunaj();
                this.cijenaBezTakse = izracun.izracunajCijenuBezTakse(distanca, tezina, hitnost);


                this.id = generisiID(1000, 9999);

                if (!Osoba.validacijaIme(i))
                    throw new ArgumentException("Ime može sadržavati samo slova!");

                this.ime = i;

                if (!Osoba.validacijaPrezime(p))
                    throw new ArgumentException("Prezime može sadržavati samo slova!");

                this.prezime = p;

                if (!Osoba.ValidacijaJMBG(j))
                    throw new ArgumentException("Pogrešan matični broj !");

                this.jmbg = j;

                if (!Osoba.ValidacijaTel(tel))
                    throw new ArgumentException("Pogrešan format telefona !");

                this.telefon = tel;

                this.komentar = kom;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        #endregion

        #region GETERI I SETERI

        public String Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        public String Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }

        public String JMBG
        {
            get { return jmbg; }
            set { jmbg = value; }
        }

        public String Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }

        public String Komentar
        {
            get { return komentar; }
            set { komentar = value; }
        }

        public Int32 Id
        {
            get { return id; }
            set { id = generisiID(1000, 9999); }
        }

        public String Odakle
        {
            get { return odakle; }
            set { odakle = value; }
        }

        public String Dokle
        {
            get { return dokle; }
            set { dokle = value; }
        }

        public Double Distanca
        {
            get { return distanca; }
            set { distanca = value; }
        }

        public Double Tezina
        {
            get { return tezina; }
            set { tezina = value; }
        }

        public Double Takse
        {
            get { return takse; }
            set { takse = value; }
        }

        public Double Hitnost
        {
            get { return hitnost; }
            set { hitnost = value; }
        }

        public Double CijenaBezTakse
        {
            get { return cijenaBezTakse; }
            set { cijenaBezTakse = value; }
        }

        public Double CijenaSaTaksom
        {
            get { return cijenaSaTaksom; }
            set { cijenaSaTaksom = value; }
        }

        #endregion

        #region FUNKCIJE
        

        public static Int32 generisiID(Int32 min, Int32 max)
        {
            lock (getrandom)
            {
                return getrandom.Next(min, max);
            }
        }

        public static Boolean validacijaOdakle(String o)
        {
            if (o.Any(char.IsDigit))
                return false;
            return true;
        }

        public static Boolean validacijaDokle(String d)
        {
            if (d.Any(char.IsDigit))
                return false;
            return true;
        }
        
        public static Boolean validacijaDistanca(String dis)
        {
            Double broj;

            if (!Double.TryParse(dis, out broj))
                throw new ArgumentException("Distanca mora biti numerička vrijednost!");
            else if (Convert.ToDouble(dis) <= 0)
                throw new ArgumentException("Distanca mora biti veća od nule!");

                return true;
        }

        public static Boolean validacijaTezina(String tez)
        {
            Double broj;

            if (!Double.TryParse(tez, out broj))
                throw new ArgumentException("Tezina mora biti numerička vrijednost!");
            else if (Convert.ToDouble(tez) <= 0)
                throw new ArgumentException("Tezina mora biti veća od nule!");

            return true;
        }

        public static Boolean validacijaTakse(String tak)
        {
            Double broj;

            if (!Double.TryParse(tak, out broj))
                throw new ArgumentException("Takse moraju biti numerička vrijednost!");
            else if (Convert.ToDouble(tak) <= 0)
                throw new ArgumentException("Takse moraju" +
                    " biti veća od nule!");

            return true;
        }

        public static Boolean validacijaCijena(String c)
        {
            Double broj;

            if (!Double.TryParse(c, out broj))
                throw new ArgumentException("Cijena mora biti numerička vrijednost!");
            else if (Convert.ToDouble(c) <= 0)
                throw new ArgumentException("Cijena mora biti veća od nule!");

            return true;
        }

        #endregion
    }
}
