﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadatak3.KonverzijaValuta;

namespace TestProjekat
{
    
    [TestClass]
    public class CurrencyConverterTest
    {
        CurrencyConverter _converter;

        [TestInitialize]
        public void setUp()
        {
            _converter = new CurrencyConverter(new DummyExchangeRate());
        }

        [TestMethod]
        public void TestConversion()
        {
            decimal amount = 100m;
            string fromCurrency = "BAM";
            string toCurrency = "EUR";

            decimal expected = 49.5m;

            Assert.AreEqual(expected, _converter.Convert(amount, fromCurrency, toCurrency));
        }

        [TestMethod]
        public void TestLargeAmountConversion()
        {
            decimal amount = 15452m;
            string fromCurrency = "BAM";
            string toCurrency = "EUR";
            decimal expected = 7726m;
            Assert.AreEqual(expected, _converter.Convert(amount, fromCurrency, toCurrency));

        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestNegativeAmount()
        {
            decimal amount = -100m;
            string fromCurrency = "BAM";
            string toCurrency = "EUR";
            _converter.Convert(amount, fromCurrency, toCurrency);

        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestInvalidExchangeRate()
        {
            CurrencyConverter converter = new CurrencyConverter(new InvalidExchangeRateGenerator());
            decimal amount = 1;
            string fromCurrency = "BAM";
            string toCurrency = "EUR";

            converter.Convert(amount, fromCurrency, toCurrency);

        }
    }
}
